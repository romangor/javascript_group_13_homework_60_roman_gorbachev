import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appChangeBackgroundDirective]'
})
export class changeBackgroundDirective {
  @Input() set appChangeBackgroundDirective(color:string){
   this.renderer.addClass(this.element.nativeElement, color);
  }

 constructor(private element: ElementRef, private renderer: Renderer2) {}
 }

