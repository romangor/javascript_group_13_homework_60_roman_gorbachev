import { Component, ElementRef, ViewChild } from '@angular/core';
import { rouletteNumber } from '../shared/number.model';
import { RouletteService } from '../shared/roulette.service';

@Component({
  selector: 'app-roulette',
  templateUrl: './roulette.component.html',
  styleUrls: ['./roulette.component.css']
})
export class RouletteComponent {
 @ViewChild ('radioBlack') radioBlack!: ElementRef;
 @ViewChild ('radioRed') radioRed!: ElementRef;
 @ViewChild ('radioZero') radioZero!: ElementRef;

  numbers: rouletteNumber[] = [];
  bet!: number;
  interval!: number;
  balance: number = this.rouletteService.balance;
  constructor(private rouletteService: RouletteService) {}

  getUserColor(){
    if(this.radioBlack.nativeElement.checked){
      return 'black';
    } if(this.radioRed.nativeElement.checked){
      return 'red';
    } if(this.radioZero.nativeElement.checked){
      return 'zero';
    } else {
      return 'unknown';
    }
  }

  onClickStart() {
    this.interval = this.rouletteService.interval;
    if(this.interval){
      return;
    } else {
      this.rouletteService.start();
      this.rouletteService.newNumber.subscribe(number => {
        const num = new rouletteNumber(number, this.rouletteService.getColor(number));
        this.numbers.push(num);
        this.balance = this.rouletteService.getBalance(this.getUserColor(), num, this.bet);
      });
    }
  }

  onClickStop() {
    this.rouletteService.stop();
  }

  onClickReset() {
    this.numbers = [];
  }

}
