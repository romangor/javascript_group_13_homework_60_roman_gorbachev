import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouletteComponent } from './roulette/roulette.component';
import { RouletteService } from './shared/roulette.service';
import { changeBackgroundDirective } from './directives/changeBackground.directive';

@NgModule({
  declarations: [
    AppComponent,
    RouletteComponent,
    changeBackgroundDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [RouletteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
