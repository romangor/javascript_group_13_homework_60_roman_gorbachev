import { EventEmitter } from '@angular/core';
import { rouletteNumber } from './number.model';

export class RouletteService {
  newNumber = new EventEmitter<number>();
  interval!: number;
  balance: number = 100;

  generateNumber(){
    return  Math.floor(Math.random() * (37));
  };

  getColor(number: number){
    if(number === 0){
      return 'zero';
    } if (number > 0 && number < 11){
      return (number % 2 === 0) ? 'black' : 'red';
    } if (number > 10 && number < 19) {
      return (number % 2 === 0) ? 'red' : 'black';
    } if (number > 18 && number < 29) {
      return (number % 2 === 0) ? 'black' : 'red';
    } if (number > 28 && number < 37) {
      return (number % 2 === 0) ? 'red' : 'black';
    }
    else {
      return 'unknown';
    }
  };

  start() {
    this.interval = setInterval(() => {
      this.newNumber.emit(this.generateNumber());
      }, 100);
    }

  stop() {
    clearInterval(this.interval)
  }

  getBalance(color: string, number: rouletteNumber, bet: number) {
    if(color === number.color){
      if(number.color === 'zero'){
       return this.balance = this.balance + bet * 35;
      } else {
       return this.balance = this.balance + bet;
      }
    } if(color !== number.color) {
      return this.balance = this.balance - bet;
    } else {
      return this.balance;
    }
  }
}
